import Container, { Service } from 'typedi';
import { Request, Response } from 'express';
import ExamplesService from '../services/examples.service';

@Service()
export class ExamplesController {
  private ExamplesService: ExamplesService = Container.get(ExamplesService);

  all = (req: Request, res: Response): void => {
    this.ExamplesService.all().then(r => res.json(r));
  };

  byId = (req: Request, res: Response): void => {
    const id = Number.parseInt(req.params['id']);
    this.ExamplesService.byId(id).then(r => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  };

  create = (req: Request, res: Response): void => {
    this.ExamplesService.create(req.body.name).then(r =>
      res
        .status(201)
        .location(`/api/v1/examples/${r.id}`)
        .json(r)
    );
  };
}
