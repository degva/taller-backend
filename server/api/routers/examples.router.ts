import Container, { Service } from 'typedi';
import { BaseRouter } from './base';
import { ExamplesController } from '../controllers/examples.controller';

@Service()
export class ExamplesRouter extends BaseRouter {
  private examplesController: ExamplesController = Container.get(
    ExamplesController
  );

  constructor() {
    super();
    this.router.get('', this.examplesController.all);
    this.router.post('', this.examplesController.create);
    this.router.get('/:id', this.examplesController.byId);
  }
}
