import './common/env';
import Server from './common/server';
import 'reflect-metadata';

async function server() {
  const s = await new Server().init();
  const port = parseInt(process.env.PORT || '8080');
  return s.listen(port);
}

export default server();
