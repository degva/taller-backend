import * as express from 'express';
import { Application } from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import * as os from 'os';
import * as cookieParser from 'cookie-parser';
import Container from 'typedi';

import installValidator from './swagger';

import l from './logger';
import { ExamplesRouter } from '../api/routers/examples.router';

const app = express();

export default class ExpressServer {
  private examplesRouter: ExamplesRouter = Container.get(ExamplesRouter);

  constructor() {
    const root = path.normalize(__dirname + '/../..');
    app.set('appPath', root + 'client');
    app.use(bodyParser.json({ limit: process.env.REQUEST_LIMIT || '100kb' }));
    app.use(
      bodyParser.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || '100kb'
      })
    );
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(express.static(`${root}/public`));
  }

  async init(): Promise<ExpressServer> {
    await this.router();

    return Promise.resolve(this);
  }

  async router(): Promise<void> {
    const routes = (app: Application): void => {
      app.use('/api/v1/examples', this.examplesRouter.router);
    };

    await installValidator(app, routes);
  }

  listen(p: string | number = process.env.PORT!!): Application {
    const welcome = (port: any) => () =>
      l.info(
        `up and running in ${process.env.NODE_ENV ||
          'development'} @: ${os.hostname()} on port: ${port}}`
      );
    http.createServer(app).listen(p, welcome(p));
    return app;
  }
}
